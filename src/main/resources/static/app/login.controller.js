(function () {
    'use strict';

    angular
    .module('app', ['ngStorage'])
    .controller('LoginController', LoginController);

    LoginController.$inject = ['$scope', '$localStorage', '$http', '$window', '$location'];

    function LoginController($scope, $localStorage, $http, $window, $location) {
        var vm = this;

        $scope.data ={};
        $localStorage.data = {};

        $scope.$storage = $localStorage;
        
        $localStorage.loggedUser = {};
        $scope.loggedUser = {};

        vm.login = login;
        vm.signUp = signUp;
        init();

        function init(){
        	
        }
        
        function login(username, password){
        	console.log("\n Verifying login: " + username);

        	$http({
        		method: 'GET',
        		url: '/login/' + username,
        		}).then(function success(response) {

        			if(!response.data)
        				{
        				$window.alert("Username not found.")
        				}
        			else{
        				console.log(response.data);
	        			if(!response.data.password.localeCompare(password))
	        				{
		        				console.log("Login sent sucessfully.");
		                    	console.log(response.data);
		                    	
		                    	if(response.data.password)
		                        $localStorage.loggedUser = response.data;
		                        $scope.loggedUser = $localStorage.loggedUser;
		                               	
		                    	console.log($scope.loggedUser);
		                    	if(!response.data.userRole.localeCompare("AUTHOR"))
		                    		$window.location.href = '/author';
		                    	else
		                    		$window.location.href = '/reader';
	        				}
	        			else
		        			{
		        				$window.alert("Invalid password")
		        			}
        			}
                	
        		}, function error(response) {
        			console.log("Login error.");
        		});
                	
        }
        
        function signUp(name, username, password, type){
        	console.log("Signing up.");
        	
        	
        	var matches = name.match(/\d+/g);
        	if (matches != null) {
        	    $window.alert('Name cannot contain digits.');
        	}
        	else if(!(!type.localeCompare("READER") || !type.localeCompare("AUTHOR")))
        	{
        		$window.alert('Please choose from READER or AUTHOR');	
        	}
        	else
	        {
	        	var url = '/login/signUp';
	        	var data = {		"id":null,
						        	"name": name,
						        	"userRole":type,
						        	"username": username,
						        	"password":password
	        	  			}
	
	        	$http.post(url, data).then(function (response) {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	
	        	}, function (response) {
	
	        		console.log("Test error.");
	
	        	});
	        	
	        }
        }
          
    }
})();
