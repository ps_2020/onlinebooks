(function () {
    'use strict';

    angular
    	.module('app', ['ngStorage'])
        .controller('AuthorController', AuthorController);

    AuthorController.$inject = ['$scope', '$localStorage', '$http', '$window', '$location'];

    function AuthorController($scope, $localStorage, $http, $window, $location) {
        var vm = this;
        
        vm.loggedAuthor = {};
        
        $scope.data = {};
        $localStorage.data = {};
        
        $scope.$storage = $localStorage;
        $scope.loggedUser = $localStorage.loggedUser;
        
        vm.loggedAuthor = $scope.loggedUser;

        console.log($scope.loggedUser);
        
        if(!$scope.loggedUser.userRole.localeCompare("AUTHOR")){

        }
        else if(!$scope.loggedUser.userRole.localeCompare("READER")){
        	$window.location.href = "/reader";
        }
        else{
        	$window.location.href = "/login";
        }
        
        vm.getLoggedAuthor = getLoggedAuthor;
        vm.writtenBooks = [];
        vm.getWrittenBooks = getWrittenBooks;
        vm.editInfo = editInfo;
        vm.selectedBookReviews = [];
        vm.getSelectedBook = getSelectedBook;
        vm.selectedBookId = 0;
        vm.selectedBookTitle = "";
        vm.publishBook = publishBook;
        
        init();

        function init(){
            getWrittenBooks();
        }
        
        function getLoggedAuthor(){
        	return $cookies.get('name');
        }
        
        function getWrittenBooks(){
        	var url = "/author/writtenBooks/" + $scope.loggedUser.id;
            var authorsPromise = $http.get(url);
            authorsPromise.then(function(response){
            	console.log("\n WrittenBooks of author: \n");
            	console.log(response.data)
                vm.writtenBooks = response.data;
            });
        }
        
        function editInfo(name, username, password){
        	console.log("Request");
        	var url = "/author/editAuthorInfo";
        	var data = {		"id":$scope.loggedUser.id,
					        	"name":name,
					        	"userRole":$scope.loggedUser.userRole,
					        	"username":username,
					        	"password":password
        	  			}

        	$http.post(url, data).then(function (response) {

        	console.log("Request sent sucessfully.");
        	console.log(response.data);
        	
        	$localStorage.loggedUser = response.data;
        	$scope.loggedUser = $localStorage.loggedUser;
        	
        	$window.location.href = '/author';

        	}, function (response) {
        		
        		console.log("Test error.");

        	});
        }
        
        function refreshReviews(bookId){
          	$http({
        		method: 'GET',
        		url: '/author/findBookReviews/' + bookId,
        		}).then(function success(response) {

                	console.log("Request sent sucessfully.\n");
                	console.log(response.data)
                	vm.selectedBookReviews = response.data;

        		}, function error(response) {
        			console.log("Test error.");
        		});
        }
        
        function getSelectedBook(bookId, bookTitle){
        	vm.selectedBookId = bookId;
        	vm.selectedBookTitle = bookTitle;
        	console.log(bookId);
        	
        	refreshReviews(bookId);
                	
        }
        
        function publishBook(title, genre){
        	console.log("Publishing book.");
        	
        	var url = '/author/publishBook';
        	var data = {	"id":null,
					        "title":title,
					        "genre":genre,
					        "authorId":$scope.loggedUser.id,
					        "authorName":$scope.loggedUser.name
        	  			}

        	$http.post(url, data).then(function (response) {

        	console.log("Request sent sucessfully.");
        	console.log(response.data)
        	
        	getWrittenBooks();

        	}, function (response) {

        		console.log("Test error.");

        	});
        }
        
        
      
    }
})();
