(function () {
    'use strict';

    angular
        .module('app', ['ngStorage'])
        .controller('TestController', TestController);

    TestController.$inject = ['$scope', '$localStorage', '$http', '$location', '$window'];
    
    function TestController($scope, $localStorage, $http, $location, $window) {
        var vm = this;
       
       vm.loggedReader = {};
        
       var id = 3;
       
        $scope.data = {};
        $localStorage.data = {};
        
        $scope.$storage = $localStorage;
        $scope.loggedUser = $localStorage.loggedUser;
        
        vm.loggedReader = $scope.loggedUser;

        console.log($scope.loggedUser);
        
        if(!$scope.loggedUser.id){
        	$window.location.href = "/";
        }
        
        vm.testLog = testLog;
        vm.testSignUp = testSignUp;
        vm.testFindAllBooks = testFindAllBooks;
        vm.testPublishBook = testPublishBook;
        vm.testNewFunction = testNewFunction;
        vm.testCreateReview = testCreateReview;
        vm.testAddBookStatus = testAddBookStatus;
        vm.testSearchBook = testSearchBook;
        init();

        function init(){
        	
        	//console.log("Blabla");
        	testLog();
        }
        
        function testLog(){
        	
        	console.log("Test");
        }
        
        function testNewFunction(newString){
        	console.log("New function");
        	
        	var url = '/author/newFunction';
        	var data = '' + newString;

        	$http.post(url, data).then(function (response) {
        			//success
        	}, function (response) {
        			//error
        	});
        }
        
        function testSignUp(){
        	console.log("Signing up.");
        	
        	var url = '/login/signUp';
        	var data = {		"id":2,
					        	"name":"First Reader",
					        	"userRole":"READER",
					        	"username":"user2",
					        	"password":"pass"
        	  			}

        	$http.post(url, data).then(function (response) {

        	console.log("Request sent sucessfully.");
        	console.log(response.data);

        	}, function (response) {

        		console.log("Test error.");

        	});
        }
    
        function testFindAllBooks(){
        	console.log("Retrieving books list.\n");

        	$http({
        		method: 'GET',
        		url: '/test/findAllBooks',
        		}).then(function success(response) {

                	console.log("Request sent sucessfully.\n");
                	console.log(response.data)

        		}, function error(response) {
        			console.log("Test error.");
        		});
                	
        }
        
        function testPublishBook(){
        	console.log("Publishing book.");
        	
        	var url = '/test/publishBook';
        	var data = {	"id":id++,
					        "title":"Second Book",
					        "genre":"ROMANCE",
					        "authorId":1,
					        "authorName":"NA"
        	  			}

        	$http.post(url, data).then(function (response) {

        	console.log("Request sent sucessfully.");
        	console.log(response.data)

        	}, function (response) {

        		console.log("Test error.");

        	});
        }
        
        function testCreateReview(){
        	console.log("Creating a review");
        	
        	var url = '/test/createReview';
        	var data = {	"id":1,
        					"details":"Very good",
        					"readerId":$scope.loggedUser.id,
        					"readerName": $scope.loggedUser.name,
        					"bookId": 2
			  			}

			$http.post(url, data).then(function (response) {
		
			console.log("Request sent sucessfully.");
			console.log(response.data)
		
			}, function (response) {
		
				console.log("Test error.");

			});
        }
        
        function testAddBookStatus(){
        	console.log("Adding a book status");
        	var url = '/test/addBookStatus';
        	
        	var data = {	"id":1,
        					"status":"READING",
        					"readerId":$scope.loggedUser.id,
        					"bookId": 2
			  			}

			$http.post(url, data).then(function (response) {
		
			console.log("Request sent sucessfully.");
			console.log(response.data)
		
			}, function (response) {
		
				console.log("Test error.");

			});
        	
        }
        
        function testSearchBook(searchBook){
        	console.log("Searching for a book");
        	
        	$http({
        		method: 'GET',
        		url: '/test/search/'+searchBook
        		}).then(function success(response) {

                	console.log("Request sent sucessfully.\n");
                	console.log(response.data)

        		}, function error(response) {
        			console.log("Test error.");
        		});
        }
    }
})();
