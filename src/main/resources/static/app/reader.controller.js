(function () {
    'use strict';

    angular
    	.module('app', ['ngStorage'])
        .controller('ReaderController', ReaderController);

    ReaderController.$inject = ['$scope', '$localStorage', '$http', '$window', '$location'];

    function ReaderController($scope, $localStorage, $http, $window, $location) {
        var vm = this;
        
        vm.loggedReader = {};
        
        $scope.data = {};
        $localStorage.data = {};
        
        $scope.$storage = $localStorage;
        $scope.loggedUser = $localStorage.loggedUser;
        
        vm.loggedReader = $scope.loggedUser;

        console.log($scope.loggedUser);
        
        if(!$scope.loggedUser.userRole.localeCompare("READER")){

        }
        else if(!$scope.loggedUser.userRole.localeCompare("AUTHOR")){
        	$window.location.href = "/author";
        }
        else{
        	$window.location.href = "/login";
        }
        
        vm.getLoggedReader = getLoggedReader;
        vm.allBooks = [];
        vm.searchBook = searchBook;
        vm.editInfo = editInfo;
        vm.findAllBooks = findAllBooks;
        vm.selectedBookId = 0;
        vm.selectedBookTitle = "";
        vm.selectedBookTxt = "";
        vm.getSelectedBook = getSelectedBook;
        vm.createReview = createReview;
        vm.selectedBookReviews = [];
        vm.addBookStatus = addBookStatus;
        
        vm.readingBooks = [];
        vm.readBooks = [];
        vm.abandonedBooks = [];
        
        vm.findReadingBooks = findReadingBooks;
        vm.findReadBooks = findReadBooks;
        vm.findAbandonedBooks = findAbandonedBooks;
        vm.generateReport = generateReport;
        vm.openBookInNewTab = openBookInNewTab;
        
        init();

        function init(){
        	findAllBooks();
        	findReadingBooks();
        	findReadBooks();
        	findAbandonedBooks();
        }
        
        function openBookInNewTab(bookId){
        	$window.open('file://' + "../images/" + vm.selectedBookId + ".txt}");
        	
        	//return '"' + "@{/../images/" + vm.selectedBookId + ".jpg}" + '"';
        }
        function getLoggedReader(){
        	return $cookies.get('name');
        }
        
        function editInfo(name, username, password){
        	console.log("Request");
        	
        	var matches = name.match(/\d+/g);
        	if (matches != null) {
        		
        	    $window.alert('Name cannot contain digits.');
        	}
        	else {
	        	var url = "/reader/editReaderInfo";
	        	var data = {		"id":$scope.loggedUser.id,
						        	"name":name,
						        	"userRole":$scope.loggedUser.userRole,
						        	"username":username,
						        	"password":password
	        	  			}
	
	        	$http.post(url, data).then(function (response) {
	
	        	console.log("Request sent sucessfully.");
	        	console.log(response.data);
	        	
	        	$localStorage.loggedUser = response.data;
	        	$scope.loggedUser = $localStorage.loggedUser;
	        	
	        	$window.location.href = '/reader';
	
	        	}, function (response) {
	        		
	        		console.log("Test error.");
	
	        	});
        	}
        }
        
        function findAllBooks(){
        	console.log("Retrieving books list.\n");

        	$http({
        		method: 'GET',
        		url: '/reader/findAllBooks',
        		}).then(function success(response) {

                	console.log("Request sent sucessfully.\n");
                	console.log(response.data)
                	vm.allBooks = response.data;

        		}, function error(response) {
        			console.log("Test error.");
        		});
                	
        }
        
        function findReadingBooks(){
        	console.log("Retrieving reading books list.\n");

        	$http({
        		method: 'GET',
        		url: '/reader/findReadingBooks/' + $scope.loggedUser.id ,
        		}).then(function success(response) {

                	console.log("Request sent sucessfully.\n");
                	console.log(response.data)
                	vm.readingBooks = response.data;

        		}, function error(response) {
        			console.log("Test error.");
        		});
                	
        }
        
        function findReadBooks(){
        	console.log("Retrieving read books list.\n");

        	$http({
        		method: 'GET',
        		url: '/reader/findReadBooks/' + $scope.loggedUser.id ,
        		}).then(function success(response) {

                	console.log("Request sent sucessfully.\n");
                	console.log(response.data)
                	vm.readBooks = response.data;

        		}, function error(response) {
        			console.log("Test error.");
        		});
                	
        }
        
        function findAbandonedBooks(){
        	console.log("Retrieving abandoned books list.\n");

        	$http({
        		method: 'GET',
        		url: '/reader/findAbandonedBooks/' + $scope.loggedUser.id ,
        		}).then(function success(response) {

                	console.log("Request sent sucessfully.\n");
                	console.log(response.data)
                	vm.abandonedBooks = response.data;

        		}, function error(response) {
        			console.log("Test error.");
        		});
                	
        }
        
        function refreshReviews(bookId){
          	$http({
        		method: 'GET',
        		url: '/reader/findBookReviews/' + bookId,
        		}).then(function success(response) {

                	console.log("Request sent sucessfully.\n");
                	console.log(response.data)
                	vm.selectedBookReviews = response.data;

        		}, function error(response) {
        			console.log("Test error.");
        		});
        }
        
        function getSelectedBook(bookId, bookTitle){
        	vm.selectedBookId = bookId;
        	vm.selectedBookTitle = bookTitle;
        	console.log(bookId);

        	refreshReviews(bookId);
                	
        }
        
        function searchBook(searchBook){
        	console.log("Searching for a book");
        	
        	$http({
        		method: 'GET',
        		url: '/reader/search/'+searchBook
        		}).then(function success(response) {

                	console.log("Request sent sucessfully.\n");
                	console.log(response.data)
                	vm.allBooks = response.data;

        		}, function error(response) {
        			console.log("Test error.");
        		});
        }
        
        function generateReport(){
        	var url = '/reader/generateReport';
        	var data =  $scope.loggedUser;
        	
        	console.log("Generating report");

        	$http.post(url, data).then(function (response) {

        	console.log("Request sent sucessfully.");
        	console.log(response.data);

        	}, function (response) {

        		console.log("Test error.");

        	});
        	
        }
        
        
        function createReview(details){
        	console.log("Creating a review" + details);
        	
        	var url = '/reader/createReview';
        	var data = {	"id": null,
        					"details": details,
        					"readerId":$scope.loggedUser.id,
        					"readerName": $scope.loggedUser.name,
        					"bookId": vm.selectedBookId
			  			}

			$http.post(url, data).then(function (response) {
		
			console.log("Request sent sucessfully.");
			console.log(response.data)
		
			}, function (response) {
		
				console.log("Test error.");

			});
        	
        	refreshReviews(vm.selectedBookId);
        }
        
        
        
        function addBookStatus(status, bookId){
        	console.log("Adding a book status");
        	var url = '/reader/addBookStatus';
        	
        	var data = {	"id":null,
        					"status":status,
        					"readerId":$scope.loggedUser.id,
        					"bookId": bookId
			  			}
        	
        	console.log(data.status);

			$http.post(url, data).then(function (response) {
		
			console.log("Request sent sucessfully.");
			console.log(response.data)
			
			findReadingBooks();
			}, function (response) {
		
				console.log("Test error.");

			});
			
			findReadingBooks();
        	
        }
        
      
    }
})();
