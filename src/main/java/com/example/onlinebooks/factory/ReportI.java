package com.example.onlinebooks.factory;

import java.util.Date;
import java.util.Set;

import com.example.onlinebooks.business.transfer.BookDTO;

public interface ReportI {
    void GenerateReport(Set<BookDTO> readingBooks, Set<BookDTO> finishedBooks, Set<BookDTO> abandonedBooks, Date date, String user);
}
