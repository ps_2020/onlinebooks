package com.example.onlinebooks.factory;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Set;

import com.example.onlinebooks.business.transfer.BookDTO;

public class ReportTXT implements ReportI{

    @Override
    public void GenerateReport(Set<BookDTO> readingBooks, Set<BookDTO> finishedBooks, Set<BookDTO> abandonedBooks, Date date, String user) {
        
    	final String file = "./Activities/Activity_" + user + ".txt";
    	try {
			writeToFile("Current progress for user: " + user + ", at: " + date.getDay() +"/06" + "/2020" + "\n", file);

			if(!readingBooks.isEmpty()) {
				writeToFile("\nCurrently reading:\n",file);
				for (BookDTO bookDTO : readingBooks) {
		            writeToFile(bookDTO, file);
		        }
			}
			
			if(!finishedBooks.isEmpty()) {
				writeToFile("\nFinished reading:\n",file);
				for (BookDTO bookDTO : finishedBooks) {
		            writeToFile(bookDTO, file);
		        }
			}
			if(!abandonedBooks.isEmpty()) {
				writeToFile("\nAbandoned reading:\n",file);
				for (BookDTO bookDTO : abandonedBooks) {
			        writeToFile(bookDTO, file); 
				}
			}
			
			writeToFile("\n[-------------------------------------------------------------------------]\n",file);
    	}catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

    }

    private static void writeToFile(BookDTO bookDTO, String fileName) throws FileNotFoundException, IOException{
        File file = new File(fileName);
        String content = bookDTO.toString();

        try (FileOutputStream fop = new FileOutputStream(file, true)) {
            // if file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            // get the content in bytes
            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            //fop.flush();
        }
    }
    
    private static void writeToFile(String content, String fileName) throws FileNotFoundException, IOException{
        File file = new File(fileName);

        try (FileOutputStream fop = new FileOutputStream(file, true)) {
            // if file doesn't exists, then create it
            if (!file.exists()) {
                file.createNewFile();
            }
            // get the content in bytes
            byte[] contentInBytes = content.getBytes();

            fop.write(contentInBytes);
            //fop.flush();
        }
    }
}
