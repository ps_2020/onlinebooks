package com.example.onlinebooks.factory;

public class ReportFactory {
    public ReportI getReport(String reportType) {

        switch(reportType) {
            case "TXT":
                return new ReportTXT();
        }
        return null;
    }
}
