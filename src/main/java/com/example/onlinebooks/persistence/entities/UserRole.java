package com.example.onlinebooks.persistence.entities;

import java.io.Serializable;

public enum UserRole implements Serializable{
	AUTHOR,
	READER
}
