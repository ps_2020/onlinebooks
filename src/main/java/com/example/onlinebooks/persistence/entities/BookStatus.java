package com.example.onlinebooks.persistence.entities;

import java.io.Serializable;

public enum BookStatus implements Serializable{
	READING,
	ABANDONED,
	READ
}
