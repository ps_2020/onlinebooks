package com.example.onlinebooks.persistence.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.onlinebooks.persistence.entities.Book;

public interface BookRepository extends JpaRepository<Book,Long>{

}
