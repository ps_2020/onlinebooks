package com.example.onlinebooks.persistence.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.onlinebooks.persistence.entities.Book;
import com.example.onlinebooks.persistence.entities.BookProgress;
import com.example.onlinebooks.persistence.entities.User;

public interface BookProgressRepository extends JpaRepository<BookProgress,Long>{
	
	public BookProgress findByReaderAndBook(User reader, Book book);
}
