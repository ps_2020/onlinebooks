package com.example.onlinebooks.persistence.repositories;
import org.springframework.data.jpa.repository.JpaRepository;

import com.example.onlinebooks.persistence.entities.Review;

public interface ReviewRepository extends JpaRepository<Review,Long>{

}
