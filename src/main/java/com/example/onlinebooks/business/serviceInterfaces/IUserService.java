package com.example.onlinebooks.business.serviceInterfaces;

import org.springframework.beans.factory.annotation.Autowired;

import com.example.onlinebooks.business.transfer.AuthorDTO;
import com.example.onlinebooks.business.transfer.BookDTO;
import com.example.onlinebooks.business.transfer.BookProgressDTO;
import com.example.onlinebooks.business.transfer.ReaderDTO;
import com.example.onlinebooks.business.transfer.ReviewDTO;
import com.example.onlinebooks.business.transfer.UserDTO;

public interface IUserService {

	UserDTO login(String username, String password);
	UserDTO signUp(UserDTO userDTO);
	AuthorDTO findAuthorDTOById(long id);
	ReaderDTO findReaderDTObyId(long id);
	UserDTO findByUsername(String username);
	UserDTO editUserInfo(UserDTO userDTO);
	ReviewDTO createReview(ReviewDTO reviewDTO);
	BookProgressDTO addBookStatus(BookProgressDTO bookProgressDTO);
	
}
