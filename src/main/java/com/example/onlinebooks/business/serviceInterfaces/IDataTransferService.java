package com.example.onlinebooks.business.serviceInterfaces;

import com.example.onlinebooks.business.transfer.AuthorDTO;
import com.example.onlinebooks.business.transfer.BookDTO;
import com.example.onlinebooks.business.transfer.BookProgressDTO;
import com.example.onlinebooks.business.transfer.ReaderDTO;
import com.example.onlinebooks.business.transfer.ReviewDTO;
import com.example.onlinebooks.business.transfer.UserDTO;
import com.example.onlinebooks.persistence.entities.Book;
import com.example.onlinebooks.persistence.entities.BookProgress;
import com.example.onlinebooks.persistence.entities.Review;
import com.example.onlinebooks.persistence.entities.User;

public interface IDataTransferService {

	public UserDTO getUserDTO(User user);
	public BookDTO getBookDTO(Book book);
	public AuthorDTO getAuthorDTO(User user);
	public ReaderDTO getReaderDTO(User user);
	public BookProgressDTO getBookProgressDTO(BookProgress bookProgress);
	public ReviewDTO getReviewDTO(Review review);
	public User getUser(UserDTO userDTO);
	public BookProgress getBookProgress(BookProgressDTO bookProgressDTO);
	public Review getReview(ReviewDTO reviewDTO);
	public Book getBook(BookDTO bookDTO);
}
