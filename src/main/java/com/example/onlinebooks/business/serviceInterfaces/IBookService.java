package com.example.onlinebooks.business.serviceInterfaces;

import java.util.List;
import java.util.Set;

import com.example.onlinebooks.business.transfer.BookDTO;
import com.example.onlinebooks.business.transfer.ReviewDTO;
import com.example.onlinebooks.persistence.entities.Book;

public interface IBookService {

	BookDTO publishBook(BookDTO bookDTO);
	Set<BookDTO> findAllBooks();
	Set<ReviewDTO> findReviewsByBook(long id);
	Set<BookDTO> search(String searchString);
	BookDTO findBookDTOById(long id);
	
}
