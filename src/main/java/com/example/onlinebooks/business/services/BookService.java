package com.example.onlinebooks.business.services;

import java.beans.Transient;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.onlinebooks.business.serviceInterfaces.IBookService;
import com.example.onlinebooks.business.serviceInterfaces.IDataTransferService;
import com.example.onlinebooks.business.transfer.BookDTO;
import com.example.onlinebooks.business.transfer.ReviewDTO;
import com.example.onlinebooks.persistence.entities.Book;
import com.example.onlinebooks.persistence.entities.Review;
import com.example.onlinebooks.persistence.entities.User;
import com.example.onlinebooks.persistence.repositories.BookRepository;
import com.example.onlinebooks.persistence.repositories.ReviewRepository;
import com.example.onlinebooks.persistence.repositories.UserRepository;

@Service
@Transactional
public class BookService implements IBookService{

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	BookRepository bookRepository;
	
	@Autowired
	ReviewRepository reviewRepository;
	
	@Autowired
	IDataTransferService dataTransferService;

	@Override
	public BookDTO publishBook(BookDTO bookDTO) {
		Book book = dataTransferService.getBook(bookDTO);	
		
		if(book != null) {
			book = bookRepository.save(book);
			
			return dataTransferService.getBookDTO(book);
		}
		
		return null;
	}
	
	@Override
	public Set<BookDTO> findAllBooks() {

		Set<BookDTO> returnSet = new HashSet<BookDTO>();
		for(Book b: bookRepository.findAll()) {
			returnSet.add(dataTransferService.getBookDTO(b));
		}
		
		return returnSet;
	}
	
	@Override
	public BookDTO findBookDTOById(long id) {
		BookDTO returnDTO = null;

		returnDTO = dataTransferService.getBookDTO(bookRepository.findById(id).get());
		
		return returnDTO;
	}
	@Override
	public Set<ReviewDTO> findReviewsByBook(long id){
		
		Set<ReviewDTO> returnSet = new HashSet<ReviewDTO>();
		for(Review review: reviewRepository.findAll()) {
			if(review.getBook().getId() == id) {
				returnSet.add(dataTransferService.getReviewDTO(review));
			}
		}
		
		return returnSet;
		
	}

	@Override
	public Set<BookDTO> search(String searchString){
		
		Set<BookDTO> returnSet = new HashSet<BookDTO>();
		for(Book book: bookRepository.findAll()) {
			if(book.getTitle().contains(searchString) || 
					book.getGenre().toString().contains(searchString) ||
					book.getAuthor().getName().contains(searchString)) {
				returnSet.add(dataTransferService.getBookDTO(book));
			}
		}
		
		return returnSet;
	}
}
