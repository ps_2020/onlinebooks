package com.example.onlinebooks.business.services;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.onlinebooks.business.serviceInterfaces.IDataTransferService;
import com.example.onlinebooks.business.serviceInterfaces.IUserService;
import com.example.onlinebooks.business.transfer.AuthorDTO;
import com.example.onlinebooks.business.transfer.BookDTO;
import com.example.onlinebooks.business.transfer.BookProgressDTO;
import com.example.onlinebooks.business.transfer.ReaderDTO;
import com.example.onlinebooks.business.transfer.ReviewDTO;
import com.example.onlinebooks.business.transfer.UserDTO;
import com.example.onlinebooks.persistence.entities.Book;
import com.example.onlinebooks.persistence.entities.BookProgress;
import com.example.onlinebooks.persistence.entities.Review;
import com.example.onlinebooks.persistence.entities.User;
import com.example.onlinebooks.persistence.entities.UserRole;
import com.example.onlinebooks.persistence.repositories.BookProgressRepository;
import com.example.onlinebooks.persistence.repositories.BookRepository;
import com.example.onlinebooks.persistence.repositories.ReviewRepository;
import com.example.onlinebooks.persistence.repositories.UserRepository;

@Service
public class UserService implements IUserService {

	@Autowired
	UserRepository userRepository;
	
	@Autowired
	BookRepository bookRepository;
	
	@Autowired
	ReviewRepository reviewRepository;
	
	@Autowired
	BookProgressRepository bookProgressRepository;
	
	@Autowired
	IDataTransferService dataTransferService;

	@Override
	public UserDTO login(String username, String password) {
		
		UserDTO returnDTO = null;
		User user = userRepository.findByUsername(username);
		

		if(user != null) {
			if(user.getPassword().contentEquals(password)){
				if(user.getUserRole().equals(UserRole.AUTHOR))
				{
					returnDTO = dataTransferService.getAuthorDTO(user);
				}
				else
				{
					returnDTO = dataTransferService.getReaderDTO(user);
				}
			}
				
		}
		
		return returnDTO;
	}
	

	@Override
	public UserDTO signUp(UserDTO userDTO) {
		UserDTO returnDTO = null;
		
		User user = dataTransferService.getUser(userDTO);
		
		if(userRepository.save(user) != null)
			returnDTO = userDTO;
		
		return returnDTO;
	}


	@Override
	public AuthorDTO findAuthorDTOById(long id) {
		AuthorDTO returnDTO = null;
		Optional<User> optionalUser = userRepository.findById(id);
		User user = optionalUser.get();
		
		if(user != null) {
			returnDTO = dataTransferService.getAuthorDTO(user);
		}
		return returnDTO;
	}

	@Override
	public ReaderDTO findReaderDTObyId(long id) {
		ReaderDTO returnDTO = null;
		Optional<User> optionalUser = userRepository.findById(id);
		User user = optionalUser.get();
		
		if(user != null) {
			returnDTO = dataTransferService.getReaderDTO(user);
		}
		return returnDTO;
	}

	@Override
	public UserDTO findByUsername(String username) {
		
		UserDTO returnDTO = null;
		
		User user = userRepository.findByUsername(username);
		if(user != null) {
			returnDTO = dataTransferService.getUserDTO(user);
		}
		else
		{
			System.out.println("User not found");
		}
		return returnDTO;
	}
	
	@Override
	public UserDTO editUserInfo(UserDTO userDTO) {
		User user = dataTransferService.getUser(userDTO);	
		
		if(user != null) {
			user = userRepository.save(user);
			
			return dataTransferService.getUserDTO(user);
		}
		
		return null;
	}
	
	@Override
	public ReviewDTO createReview(ReviewDTO reviewDTO) {
		Review review = dataTransferService.getReview(reviewDTO);
		
		if(review != null) {
			review = reviewRepository.save(review);
			
			return dataTransferService.getReviewDTO(review);
		}

		return null;
	}
	
	@Override
	public BookProgressDTO addBookStatus(BookProgressDTO bookProgressDTO) {
		
		BookProgress bookProgress = dataTransferService.getBookProgress(bookProgressDTO);
		
		System.out.println( bookProgressDTO.getStatus());
		if(bookProgress != null) {
			bookProgress = bookProgressRepository.save(bookProgress);
			
			return dataTransferService.getBookProgressDTO(bookProgress);
		}
		
		return null;
	}


}
