package com.example.onlinebooks.business.transfer;

import javax.persistence.Column;

import com.example.onlinebooks.persistence.entities.Book;
import com.example.onlinebooks.persistence.entities.BookStatus;
import com.example.onlinebooks.persistence.entities.User;

public class BookProgressDTO {

	private long id;
	private BookStatus status;
	private long readerId;
	private long bookId;
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public BookStatus getStatus() {
		return status;
	}
	public void setStatus(BookStatus status) {
		this.status = status;
	}
	public long getReaderId() {
		return readerId;
	}
	public void setReaderId(long readerId) {
		this.readerId = readerId;
	}
	public long getBookId() {
		return bookId;
	}
	public void setBookId(long bookId) {
		this.bookId = bookId;
	}

	
	
	
}
