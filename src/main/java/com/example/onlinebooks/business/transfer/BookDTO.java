package com.example.onlinebooks.business.transfer;

import javax.persistence.Column;

import com.example.onlinebooks.persistence.entities.Genre;

public class BookDTO {

	private long id;
	private String title;
	private Genre genre;
	private long authorId;
	private String authorName;
	private String imagePath = "";
	
	public String getImagePath() {
		return imagePath;
	}
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public Genre getGenre() {
		return genre;
	}
	public void setGenre(Genre genre) {
		this.genre = genre;
	}
	public long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(long authorId) {
		this.authorId = authorId;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

    @Override
    public String toString() {
        return  "Title: '" + title + '\'' +
                ", Genre: '" + genre + '\'' +
                ", Author: '" + authorName + '\'' +
                "\n";
    }

    public String toCSV(){
        return  title +
                "," + genre +
                "," + authorName +
                "\n";
    }
	
	
}
