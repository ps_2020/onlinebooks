package com.example.onlinebooks.business.transfer;

public class ReviewDTO {
	
	private long id;
	private String details;
	private long readerId;
	private String readerName;
	private long bookId;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public String getReaderName() {
		return readerName;
	}
	public void setReaderName(String readerName) {
		this.readerName = readerName;
	}
	public long getReaderId() {
		return readerId;
	}
	public void setReaderId(long readerId) {
		this.readerId = readerId;
	}
	public long getBookId() {
		return bookId;
	}
	public void setBookId(long bookId) {
		this.bookId = bookId;
	}
	@Override
	public String toString() {
		return "ReviewDTO [id=" + id + ", details=" + details + ", readerId=" + readerId + ", readerName=" + readerName
				+ ", bookId=" + bookId + "]";
	}
	
	
	
	

}
