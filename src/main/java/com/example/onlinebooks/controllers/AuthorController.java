package com.example.onlinebooks.controllers;

import java.util.ArrayList;
import java.util.Set;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.pojo.ApiStage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.onlinebooks.business.services.BookService;
import com.example.onlinebooks.business.services.UserService;
import com.example.onlinebooks.business.transfer.BookDTO;
import com.example.onlinebooks.business.transfer.ReviewDTO;
import com.example.onlinebooks.business.transfer.UserDTO;

@RestController
@RequestMapping(value = "/author")
@Api(
        name = "Author API",
        description = "Provides author related methods",
        stage = ApiStage.RC)
public class AuthorController {
	
	@Autowired
	UserService userService;
	
	@Autowired
	BookService bookService;
	
	@RequestMapping(value = "/writtenBooks/{id}", method = RequestMethod.GET)
    @ApiMethod(description = "Get written books of author with requested id")
    public Set<BookDTO> getWrittenBooks(@ApiPathParam(name = "id") @PathVariable long id){
		
		System.out.println("getWrittenBooks get: " + id + '\n');
		Object[] books = userService.findAuthorDTOById(id).getWrittenBooks().toArray();
		
    	return userService.findAuthorDTOById(id).getWrittenBooks();    	
    }
	
	@RequestMapping(value = "/newFunction", method = RequestMethod.POST)
	public void printNewFunction(@RequestBody String newFunction) {
		System.out.println(newFunction);
	}
	
	@RequestMapping(value = "/editAuthorInfo", method = RequestMethod.POST)
	public UserDTO editAuthorInfo(@RequestBody UserDTO userDTO) {
		UserDTO returnDTO = null;
		
		if(userDTO != null) {
			returnDTO = userService.editUserInfo(userDTO);
		}
		
		return returnDTO;
	}
	
	@RequestMapping(value = "/findBookReviews/{bookId}", method = RequestMethod.GET)
	 public Set<ReviewDTO> getBookReviews(@ApiPathParam(name = "bookId") @PathVariable long bookId){
		
		System.out.println("Searching for reviews");
		
		return bookService.findReviewsByBook(bookId);
	}
	
	@RequestMapping(value = "/publishBook", method = RequestMethod.POST)
    @ApiMethod(description = "Stores a book in the database")
    public BookDTO publishBook(@RequestBody BookDTO bookDTO){
	
		BookDTO returnDTO = null;
		System.out.println("Publishing book.");
		
		if(bookDTO != null)
			returnDTO = bookService.publishBook(bookDTO);
		System.out.println(returnDTO);
    	return returnDTO;
    }
}
