package com.example.onlinebooks.controllers;

import org.jsondoc.core.annotation.ApiMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;


import java.util.Date;
import java.util.List;

@Controller
public class ViewController {
    private String appMode;

    @Autowired
    public ViewController(Environment environment){
        appMode = environment.getProperty("app-mode");
    }
    
    @RequestMapping("/")
    public String index(Model model){
        model.addAttribute("mode", appMode);
        return "index";
    }

    @RequestMapping("/login")
    public String login(Model model){
        model.addAttribute("mode", appMode);
        return "index";
    }
    
    @RequestMapping("/test")
    public String test(Model model){
        model.addAttribute("mode", appMode);
        return "test";
    }
    
    
    @RequestMapping("/author")
    public String author(Model model) {
    	model.addAttribute("mode", appMode);
    	
    	return "author";
    }
    
    @RequestMapping("/reader")
    public String reader(Model model) {
    	model.addAttribute("mode", appMode);
    	
    	return "reader";
    }

}
