package com.example.onlinebooks.controllers;

import java.util.Set;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.pojo.ApiStage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.onlinebooks.business.services.BookService;
import com.example.onlinebooks.business.services.UserService;
import com.example.onlinebooks.business.transfer.BookDTO;
import com.example.onlinebooks.business.transfer.BookProgressDTO;
import com.example.onlinebooks.business.transfer.ReviewDTO;
import com.example.onlinebooks.business.transfer.UserDTO;

@RestController
@RequestMapping(value = "/test")
@Api(
        name = "Test API",
        description = "Test",
        stage = ApiStage.RC)
public class TestController {

	/*
	@Autowired
	BookProgressService bookProgressService;

	@Autowired
	ReviewService reviewService;
	*/
	
	@Autowired
	UserService userService;
	
	@Autowired
	BookService bookService;
	

	
	@RequestMapping(value = "/findAllBooks", method = RequestMethod.GET)
    @ApiMethod(description = "Retrieves the books from the database")
    public Set<BookDTO> findAllBooks(){
		
		System.out.println("Retreiving books list.");
		
    	return bookService.findAllBooks();
    }
	
	@RequestMapping(value = "/publishBook", method = RequestMethod.POST)
    @ApiMethod(description = "Stores a book in the database")
    public BookDTO publishBook(@RequestBody BookDTO bookDTO){
	
		BookDTO returnDTO = null;
		System.out.println("Publishing book.");
		
		if(bookDTO != null)
			returnDTO = bookService.publishBook(bookDTO);
		System.out.println(returnDTO);
		
    	return returnDTO;
    }
	
	@RequestMapping(value = "/createReview", method = RequestMethod.POST)
	public ReviewDTO createReview(@RequestBody ReviewDTO reviewDTO) {
		
		ReviewDTO returnDTO = null;
		System.out.println("Creating a review");
		if(reviewDTO != null) {
			returnDTO = userService.createReview(reviewDTO);
		}
		System.out.println(returnDTO);
		return returnDTO;
	}
	
	@RequestMapping(value = "/addBookStatus", method = RequestMethod.POST)
	public BookProgressDTO addBookStatus(@RequestBody BookProgressDTO bookProgressDTO) {
		
		BookProgressDTO returnDTO = null;
		System.out.println("Adding a book status");
		
		if(bookProgressDTO != null) {
			returnDTO = userService.addBookStatus(bookProgressDTO);
		}
		System.out.println(returnDTO);
		return returnDTO;
	}
	
	@RequestMapping(value = "/search/{searchBook}", method = RequestMethod.GET)
	 public Set<BookDTO> searchBook(@ApiPathParam(name = "searchBook") @PathVariable String searchBook){
		
		System.out.println("Searching for a book");
		
		return bookService.search(searchBook);
	}
	
}
