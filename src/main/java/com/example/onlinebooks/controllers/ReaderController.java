package com.example.onlinebooks.controllers;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.jsondoc.core.pojo.ApiStage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.onlinebooks.business.services.BookService;
import com.example.onlinebooks.business.services.UserService;
import com.example.onlinebooks.business.transfer.BookDTO;
import com.example.onlinebooks.business.transfer.BookProgressDTO;
import com.example.onlinebooks.business.transfer.ReaderDTO;
import com.example.onlinebooks.business.transfer.ReviewDTO;
import com.example.onlinebooks.business.transfer.UserDTO;
import com.example.onlinebooks.factory.ReportFactory;
import com.example.onlinebooks.factory.ReportI;
import com.example.onlinebooks.persistence.entities.BookStatus;

@RestController
@RequestMapping(value = "/reader")
@Api(
        name = "Reader API",
        description = "Reader",
        stage = ApiStage.RC)
public class ReaderController {
	
	
	@Autowired
	UserService userService;
	
	@Autowired
	BookService bookService;
	

	
	@RequestMapping(value = "/findAllBooks", method = RequestMethod.GET)
    @ApiMethod(description = "Retrieves the books from the database")
    public Set<BookDTO> findAllBooks(){
		
		System.out.println("Retreiving books list.");
		
    	return bookService.findAllBooks();
    }
	
	@RequestMapping(value = "/findReadingBooks/{readerId}", method = RequestMethod.GET)
	 public Set<BookDTO> findReadingBooks(@ApiPathParam(name = "readerId") @PathVariable long readerId){
		
		System.out.println("All reading books for reader");
		
		Set<BookDTO> returnSet = new HashSet<BookDTO>();
		ReaderDTO readerDTO =  userService.findReaderDTObyId(readerId);
		if(readerDTO != null)
		{
			if(readerDTO.getBooksInProgress() != null)
			for(BookProgressDTO bookProgressDTO : readerDTO.getBooksInProgress() ) {
				if(bookProgressDTO.getStatus() == BookStatus.READING) {
					returnSet.add(bookService.findBookDTOById(bookProgressDTO.getBookId()));
				}
			}
		}
		return returnSet;
	}
	
	@RequestMapping(value = "/findReadBooks/{readerId}", method = RequestMethod.GET)
	 public Set<BookDTO> findReadBooks(@ApiPathParam(name = "readerId") @PathVariable long readerId){
		
		System.out.println("All read books for reader");
		
		Set<BookDTO> returnSet = new HashSet<BookDTO>();
		ReaderDTO readerDTO =  userService.findReaderDTObyId(readerId);
		if(readerDTO != null)
		{
			if(readerDTO.getBooksInProgress() != null)
			for(BookProgressDTO bookProgressDTO : readerDTO.getBooksInProgress() ) {
				if(bookProgressDTO.getStatus() == BookStatus.READ) {
					returnSet.add(bookService.findBookDTOById(bookProgressDTO.getBookId()));
				}
			}
		}
		return returnSet;
	}
	
	@RequestMapping(value = "/findAbandonedBooks/{readerId}", method = RequestMethod.GET)
	 public Set<BookDTO> findAbandonedBooks(@ApiPathParam(name = "readerId") @PathVariable long readerId){
		
		System.out.println("All abandoned books for reader");
		
		Set<BookDTO> returnSet = new HashSet<BookDTO>();
		ReaderDTO readerDTO =  userService.findReaderDTObyId(readerId);
		if(readerDTO != null)
		{
			if(readerDTO.getBooksInProgress() != null)
			for(BookProgressDTO bookProgressDTO : readerDTO.getBooksInProgress() ) {
				if(bookProgressDTO.getStatus() == BookStatus.ABANDONED) {
					returnSet.add(bookService.findBookDTOById(bookProgressDTO.getBookId()));
				}
			}
		}
		return returnSet;
	}
	
	@RequestMapping(value = "/search/{searchBook}", method = RequestMethod.GET)
	 public Set<BookDTO> searchBook(@ApiPathParam(name = "searchBook") @PathVariable String searchBook){
		
		System.out.println("Searching for a book");
		
		return bookService.search(searchBook);
	}
	
	@RequestMapping(value = "/createReview", method = RequestMethod.POST)
	public ReviewDTO createReview(@RequestBody ReviewDTO reviewDTO) {
		
		ReviewDTO returnDTO = null;
		System.out.println("Creating a review");
		if(reviewDTO != null) {
			returnDTO = userService.createReview(reviewDTO);
		}
		System.out.println(returnDTO);
		return returnDTO;
	}
	
	@RequestMapping(value = "/addBookStatus", method = RequestMethod.POST)
	public BookProgressDTO addBookStatus(@RequestBody BookProgressDTO bookProgressDTO) {
		
		BookProgressDTO returnDTO = null;
		System.out.println("Adding a book status");
		
		if(bookProgressDTO != null) {
			returnDTO = userService.addBookStatus(bookProgressDTO);
		}
		System.out.println(returnDTO);
		return returnDTO;
	}
	
	@RequestMapping(value = "/findBookReviews/{bookId}", method = RequestMethod.GET)
	 public Set<ReviewDTO> getBookReviews(@ApiPathParam(name = "bookId") @PathVariable long bookId){
		
		System.out.println("Searching for reviews");
		
		return bookService.findReviewsByBook(bookId);
	}
	
	@RequestMapping(value = "/editReaderInfo", method = RequestMethod.POST)
	public UserDTO editReaderInfo(@RequestBody UserDTO userDTO) {
		UserDTO returnDTO = null;
		
		if(userDTO != null) {
			returnDTO = userService.editUserInfo(userDTO);
		}
		
		return returnDTO;
	}
	
	@RequestMapping(value = "/generateReport", method = RequestMethod.POST)
    public int generateReport(@RequestBody UserDTO userDTO){
            ReportFactory reportFactory = new ReportFactory();
            Set<BookProgressDTO> bookProgressDTOs = userService.findReaderDTObyId(userDTO.getId()).getBooksInProgress();
            
            Set<BookDTO> readingBooks = new HashSet<BookDTO>();
            Set<BookDTO> finishedBooks = new HashSet<BookDTO>();
            Set<BookDTO> abandonedBooks = new HashSet<BookDTO>();
            
            System.out.println("blabla");
            Collection<BookProgressDTO> readingProgress = bookProgressDTOs
                    .stream()
                    .filter(b -> b.getStatus() == BookStatus.READING)
                    .collect(Collectors.toSet());
            Collection<BookProgressDTO> finishedProgress = bookProgressDTOs
                    .stream()
                    .filter(b -> b.getStatus() == BookStatus.READ)
                    .collect(Collectors.toSet());
            Collection<BookProgressDTO> abandonedProgress = bookProgressDTOs
                    .stream()
                    .filter(b -> b.getStatus() == BookStatus.ABANDONED)
                    .collect(Collectors.toSet());
            
            for(BookProgressDTO b: readingProgress) {
            	readingBooks.add(bookService.findBookDTOById(b.getBookId()));
            }
            for(BookProgressDTO b: finishedProgress) {
            	finishedBooks.add(bookService.findBookDTOById(b.getBookId()));
            }
            for(BookProgressDTO b: abandonedProgress) {
            	abandonedBooks.add(bookService.findBookDTOById(b.getBookId()));
            }
            
            ReportI reportTxt = reportFactory.getReport("TXT");
            reportTxt.GenerateReport(readingBooks, finishedBooks, abandonedBooks, new Date(), userDTO.getName());
            
         
            return 1;

    }
	

}
